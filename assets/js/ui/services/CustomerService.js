(function() {
    var module = angular.module('ERS.services');

    var CustomerService = function($http,localStorageService) {
        var addCustomer = function(name, location, email) {
            return {
                name: name,
                location: location,
                email: email
            };
        };

        var getCustomers = function() {
            var customers = localStorageService.get('customers');

            if(customers) {
                return customers
            } else {
                return $http.get('customerDummy.json');
            }
        };


        var saveCustomers = function(customers) {
            localStorageService.set('customers', customers);
        };

        return {
            addCustomer: addCustomer,
            getCustomers: getCustomers,
            saveCustomers: saveCustomers
        }
    };

    module.service('CustomerService', ['$http','localStorageService',CustomerService]);

})(this.angular);