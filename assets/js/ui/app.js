(function() {
    var app = angular.module('ERS',
        [
            'ui.bootstrap',
            'ui.router',
            'LocalStorageModule',
            'ngMessages',
            'ERS.services',
            'ERS.controllers'
        ]
    );

    app.config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/login');

        $stateProvider

            // HOME STATES AND NESTED VIEWS ========================================
            .state('login', {
                url: '/login',
                templateUrl: '/ui/templates/login.tpl.html'
            })

            // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
            .state('register', {
                url: '/register',
                templateUrl: '/ui/templates/addCustomer.tpl.html'
            });

    });
})(this.angular);
