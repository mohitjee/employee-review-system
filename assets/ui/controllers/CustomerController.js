(function() {
    var module = angular.module('ERS.controllers');

    var CustomerController = function($scope, $uibModal, $filter, CustomerService) {
        var model;
        var viewHelpers;
        var modalInstance;

        $scope.model = {};
        $scope.viewHelpers = {};

        model = $scope.model;
        viewHelpers = $scope.viewHelpers;

        model.customers = [];
        model.newCustomer = {};
        model.currentCustomer = {};

        viewHelpers.sortOrder = "-name";

        getCustomers();

        $scope.openAddUserPanel = function() {
            modalInstance = $uibModal.open({
                templateUrl: 'templates/addCustomer.tpl.html',
                size: 'md',
                scope: $scope
            });

        };

        $scope.getSortOrder = function() {
            console.log(viewHelpers.sortOrder);
            return viewHelpers.sortOrder;
        };

        $scope.setCurrentCustomer = function(customer) {
            model.currentCustomer = customer;
        };

        $scope.deleteCustomer = function(customer) {

            var currentCustomer = model.currentCustomer;

            if(angular.equals(customer, currentCustomer)) {
                model.currentCustomer = {};
            }

            var index = model.customers.indexOf(customer);
            model.customers.splice(index, 1);

            CustomerService.saveCustomers(model.customers);


        };

        $scope.addCustomer = function(newCustomerDetails) {
            var newCustomer = CustomerService.addCustomer(newCustomerDetails.name, newCustomerDetails.location, newCustomerDetails.email);
            model.customers.push(newCustomer);
            console.log('newCustomer', newCustomer);
            console.log('model.customers', model.customers);
            CustomerService.saveCustomers(model.customers);

            $scope.resetNewCustomer();
        };

        $scope.resetNewCustomer = function() {
            modalInstance.dismiss();
            model.newCustomer = {};
        };

        function getCustomers() {
            var customers = CustomerService.getCustomers();

            if(customers.success) {
                customers.success(getCustomerCallback);
            } else {
                model.customers = customers;
            }
        }

        function getCustomerCallback(response) {
            model.customers = response;
            CustomerService.saveCustomers(response);
        }

    };

    module.controller('CustomerController', ['$scope', '$uibModal', '$filter', 'CustomerService', CustomerController]);

})(this.angular);