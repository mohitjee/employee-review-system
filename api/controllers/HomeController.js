var HomeController = {
    home: function (req, res) {
        return res.view('homepage', {}, function(err, html) {
            return res.send(html);
        });
    }
};

module.exports = HomeController;