/**
 * FormController
 *
 * @description :: Server-side logic for managing forms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    read: function(req, res) {
        var user = req.user;

        Form.find({user: user[0].id}, function(err, forms) {
            if (err) {
                res.send(400);
            }

            res.json(forms);
        });
    },

    create: function(req, res) {
        var user = req.user,
            title = req.param('title'),
            description = req.param('description');

        Form.create({
            user: user[0].id,
            title: title,
            description: description
        }, function(err, form) {
           if (err) {
               res.serverError(err);
           }

            res.ok({'created': true});
        });

    }
};

